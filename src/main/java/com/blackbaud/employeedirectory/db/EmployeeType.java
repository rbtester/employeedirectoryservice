package com.blackbaud.employeedirectory.db;

public enum EmployeeType {
    HR,
    REGULAR
}
